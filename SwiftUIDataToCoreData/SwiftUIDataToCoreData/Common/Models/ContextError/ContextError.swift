//
//  ContextError.swift
//  SwiftUIDataToCoreData
//
//  Created by Alexey Khomych on 03.03.2023.
//

import Foundation

enum ContextError: Error {
    case noContextFound
}
