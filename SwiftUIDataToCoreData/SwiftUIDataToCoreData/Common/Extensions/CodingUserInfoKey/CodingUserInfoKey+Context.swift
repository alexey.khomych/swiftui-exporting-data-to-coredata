//
//  CodingUserInfoKey+Context.swift
//  SwiftUIDataToCoreData
//
//  Created by Alexey Khomych on 03.03.2023.
//

import Foundation

extension CodingUserInfoKey {
    
    static let context = CodingUserInfoKey(rawValue: "managedObjectContext")!
}
