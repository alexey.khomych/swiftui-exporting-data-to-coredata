//
//  Purchase+CoreDataClass.swift
//  SwiftUIDataToCoreData
//
//  Created by Alexey Khomych on 03.03.2023.
//
//

import Foundation
import CoreData

@objc(Purchase)
public class Purchase: NSManagedObject, Codable {
    
    enum CodingKeys: CodingKey {
        case id
        case title
        case dateOfPurchase
        case amountSpent
    }
    
    required convenience public init(from decoder: Decoder) throws {
        guard let context = decoder.userInfo[.context] as? NSManagedObjectContext else {
            throw ContextError.noContextFound
        }
        
        self.init(context: context)
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(UUID.self, forKey: .id)
        title = try values.decode(String.self, forKey: .title)
        dateOfPurchase = try values.decode(Date.self, forKey: .dateOfPurchase)
        amountSpent = try values.decode(Double.self, forKey: .amountSpent)
    }
    
    public func encode(to encoder: Encoder) throws {
        var values = encoder.container(keyedBy: CodingKeys.self)
        try values.encode(id, forKey: .id)
        try values.encode(title, forKey: .title)
        try values.encode(dateOfPurchase, forKey: .dateOfPurchase)
        try values.encode(amountSpent, forKey: .amountSpent)
    }
}
