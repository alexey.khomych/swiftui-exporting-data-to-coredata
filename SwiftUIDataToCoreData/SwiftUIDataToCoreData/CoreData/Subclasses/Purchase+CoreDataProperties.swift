//
//  Purchase+CoreDataProperties.swift
//  SwiftUIDataToCoreData
//
//  Created by Alexey Khomych on 03.03.2023.
//
//

import Foundation
import CoreData


extension Purchase {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Purchase> {
        return NSFetchRequest<Purchase>(entityName: "Purchase")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var title: String?
    @NSManaged public var dateOfPurchase: Date?
    @NSManaged public var amountSpent: Double

}

extension Purchase : Identifiable {

}
