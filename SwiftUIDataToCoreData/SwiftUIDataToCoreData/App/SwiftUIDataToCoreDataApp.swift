//
//  SwiftUIDataToCoreDataApp.swift
//  SwiftUIDataToCoreData
//
//  Created by Alexey Khomych on 03.03.2023.
//

import SwiftUI

@main
struct SwiftUIDataToCoreDataApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
