//
//  AddNewExpense.swift
//  SwiftUIDataToCoreData
//
//  Created by Alexey Khomych on 03.03.2023.
//

import SwiftUI

struct AddNewExpense: View {
    
    // MARK: - Private Variables
    
    @State private var title = ""
    @State private var dateOfPurchase = Date()
    @State private var amountSpent = 0.0
    
    @Environment(\.dismiss) private var dismiss
    @Environment(\.managedObjectContext) private var context
    
    // MARK: - Public Variables
    
    var body: some View {
        NavigationStack {
            List {
                Section("Purchase Item") {
                    TextField("", text: $title)
                }
                Section("Date of purchase") {
                    DatePicker("", selection: $dateOfPurchase, displayedComponents: [.date])
                }
                Section("Amount Spent") {
                    TextField(value: $amountSpent, formatter: currencyFormatter) {
                        
                    }
                    .labelsHidden()
                    .keyboardType(.numberPad)
                }
            }
            .navigationTitle("New Expense")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button("Add") {
                        addExpense()
                    }
                }
                
                ToolbarItem(placement: .navigationBarLeading) {
                    Button("Cancel") {
                        dismiss()
                    }
                }
            }
        }
    }
}

// MARK: - Private

private extension AddNewExpense {
    
    func addExpense() {
        do {
            let purchase = Purchase(context: context)
            purchase.id = .init()
            purchase.title = title
            purchase.dateOfPurchase = dateOfPurchase
            purchase.amountSpent = amountSpent
            
            try context.save()
            
            dismiss()
        } catch {
            print(error.localizedDescription)
        }
    }
}

// MARK: - PreviewProvider

struct AddNewExpense_Previews: PreviewProvider {
    static var previews: some View {
        AddNewExpense()
    }
}
