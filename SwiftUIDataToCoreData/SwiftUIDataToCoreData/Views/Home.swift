//
//  Home.swift
//  SwiftUIDataToCoreData
//
//  Created by Alexey Khomych on 03.03.2023.
//

import SwiftUI
import CoreData

struct Home: View {
    
    // MARK: - Private Variables
    
    @State private var addExpense = false
    @FetchRequest(
        entity: Purchase.entity(),
        sortDescriptors: [
            NSSortDescriptor(keyPath: \Purchase.dateOfPurchase, ascending: false)
        ],
        animation: .easeInOut(duration: 0.3)
    ) private var purchasedItem: FetchedResults<Purchase>
    
    @Environment(\.managedObjectContext) private var context
    
    @State private var presentShareSheet = false
    @State private var shareUrl = URL(string: "https://apple.com")!
    @State private var presentFilePicker = false
    
    // MARK: - Public Variables
    
    var body: some View {
        NavigationStack {
            List {
                ForEach(purchasedItem) { purchase in
                    HStack(spacing: 10) {
                        VStack(alignment: .leading, spacing: 6) {
                            Text(purchase.title ?? "")
                                .fontWeight(.semibold)
                            Text((purchase.dateOfPurchase ?? .init())
                                .formatted(date: .abbreviated, time: .omitted))
                            .font(.caption)
                            .foregroundColor(.gray)
                        }
                        Spacer(minLength: 0)
                        Text(currencyFormatter.string(from: NSNumber(value: purchase.amountSpent)) ?? "")
                            .fontWeight(.bold)
                    }
                }
            }
            .listStyle(.insetGrouped)
            .navigationTitle("My Expenses")
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button {
                        addExpense.toggle()
                    } label: {
                        Image(systemName: "plus")
                    }
                }
                ToolbarItem(placement: .navigationBarLeading) {
                    Menu {
                        Button("Import") {
                            presentShareSheet.toggle()
//                            importJson()
                        }
                        Button("Export", action: exportCoreData)
                    } label: {
                        Image(systemName: "ellipsis")
                            .rotationEffect(.init(degrees: -90))
                    }
                }
            }
            .sheet(isPresented: $addExpense) {
                AddNewExpense()
                    .presentationDetents([.medium])
                    .interactiveDismissDisabled()
            }
            .sheet(isPresented: $presentShareSheet) {
                deleteTempData()
            } content: {
                CustomShareSheet(url: $shareUrl)
            }
            .fileImporter(isPresented: $presentShareSheet, allowedContentTypes: [.json]) { result in
                switch result {
                case let .success(success):
                    importJson(url: success)
                case let .failure(failure):
                    print(failure.localizedDescription)
                }
            }
        }
    }
}

// MARK: - Private

private extension Home {
    
    func exportCoreData() {
        do {
            if let entityName = Purchase.entity().name {
                let request = NSFetchRequest<NSFetchRequestResult>(
                    entityName: entityName
                )
                let items = try context.fetch(request).compactMap {
                    $0 as? Purchase
                }
                
                convertDataToJSON(items: items)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func importJson(url: URL) {
        do {
            let jsonData = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            decoder.userInfo[.context] = context
            let items = try decoder.decode([Purchase].self, from: jsonData)
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func convertDataToJSON(items: [Purchase]) {
        do {
            let jsonData = try JSONEncoder().encode(items)
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                if let tempUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                    let pathUrl = tempUrl.appending(component: "Export \(Date().formatted(date: .complete, time: .omitted)).json")
                    try jsonString.write(
                        to: pathUrl,
                        atomically: true,
                        encoding: .utf8
                    )
                    
                    shareUrl = pathUrl
                    presentShareSheet.toggle()
                }
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func deleteTempData() {
        do {
            try FileManager.default.removeItem(at: shareUrl)
            print("file removed")
        } catch {
            print(error.localizedDescription)
        }
    }
}

// MARK: - PreviewProvider

struct Home_Previews: PreviewProvider {
    
    static var previews: some View {
        ContentView()
    }
}
