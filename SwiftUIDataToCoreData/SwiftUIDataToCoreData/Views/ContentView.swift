//
//  ContentView.swift
//  SwiftUIDataToCoreData
//
//  Created by Alexey Khomych on 03.03.2023.
//

import SwiftUI

struct ContentView: View {

    // MARK: - Public Variables
    
    var body: some View {
        Home()
    }
}

// MARK: - PreviewProvider

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
