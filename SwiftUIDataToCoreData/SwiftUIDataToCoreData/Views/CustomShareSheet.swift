//
//  CustomShareSheet.swift
//  SwiftUIDataToCoreData
//
//  Created by Alexey Khomych on 03.03.2023.
//

import SwiftUI

struct CustomShareSheet: UIViewControllerRepresentable {
    
    // MARK: - Public Variables
    
    @Binding var url: URL
    
    // MARK: - Public
    
    func makeUIViewController(context: Context) -> UIActivityViewController {
        return UIActivityViewController(activityItems: [url], applicationActivities: nil)
    }
    
    func updateUIViewController(_ uiViewController: UIActivityViewController, context: Context) {
        
    }
}
